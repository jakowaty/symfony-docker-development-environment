## PHP8 Symfony RabbitMQ Bundle (in ./application/lib/Piotrbe/RabbitMqBundle)


### Running application (you need docker and docker-compose installed)
``(in main project directory follow)``

####Firing up application: 


1. docker-compose up


2. docker ps (find id/name of `php_fpm` container) and copy it


3. docker exec -it <container id/name from 2> bash


4. in container `# composer install` and `# exit`

####Publishing messages:


1. At http://127.0.0.1:18080/, TestController will push two messages into queue


2. You can check it http://127.0.0.1:15672 (login with: guest/guest)

#### Consuming messages:


1. docker ps (find id/name of `php_cli` container) and copy it


2. docker exec -it <id/name from 1> bash


3. in container `/var/www/html# bin/console consumer:consume MESSAGE_TYPE`


#### Running tests


1. docker ps (find id/name of `php_cli` container) and copy it


2. docker exec -it <id/name from 1> bash


3. in container `/var/www/html# bin/phpunit`